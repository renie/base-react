# Base project

## Setup

### Clone

```bash
git clone git@gitlab.com:renie/base-react.git
cd base-react
```

### Install deps

```bash
npm i
```

### Run

```bash
npm run dev
```