import AddTodoItem from './components/Todo/AddTodoItem'
import TodoList from './components/Todo/TodoList'
import useTodoList from './hook/useTodoList'
import styles from './app.module.css'

const App = () => {

  const { todoList, addTodoItem, removeTodoItem } = useTodoList()

  const addItemToList = (item) => addTodoItem(item)

  const removeItemFromList = (id) => removeTodoItem(id)

  return (
    <>
      <div className={styles.wrapper}>
        <h1 className="title">Base app</h1>
        <AddTodoItem
          setNewItem={addItemToList} />
        <TodoList
          items={todoList}
          deleteFunction={removeItemFromList} />
      </div>
    </>
  )
}

export default App
