import { useState } from "react"

const AddTodoItem = ({setNewItem}) => {

    const [ itemValue, setItemValue ] = useState('')
    const [ disabled, setDisabled ] = useState(true)

    const addItem = () => {
        setNewItem(itemValue)
        setItemValue('')
        setDisabled(true)
    }

    const setStateValue = ({target}) => {
        setDisabled(target.value.trim() === '')
        setItemValue(target.value)
    }
    
    const checkReturnkey = ({key}) => 
        (key === 'Enter' && !disabled) && addItem()
    
    return (
        <div className="field has-addons">
            <div className="control is-expanded">
                <input
                    className="input is-normal"
                    type="text"
                    placeholder="New Todo"
                    value={itemValue}
                    onChange={setStateValue}
                    onKeyUp={checkReturnkey} />
            </div>
            <div className="control">
                <button
                    className="button is-info"
                    onClick={addItem}
                    disabled={disabled}
                    >+</button>
            </div>
        </div>
    )
}

export default AddTodoItem