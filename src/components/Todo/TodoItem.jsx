import styles from './todoItem.module.css'

const TodoItem = ({value, id, deleteFunction}) => {
    const removeItem = () => deleteFunction(id)

    return (
        <li className={styles.todoItem}>
            <p>{value}</p>
            <button
                className="button is-warning"
                onClick={removeItem}>&#215;</button>
        </li>
    )
}

export default TodoItem