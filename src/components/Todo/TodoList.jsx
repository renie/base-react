import TodoItem from './TodoItem'
import styles from './todoList.module.css'

const TodoList = ({items, deleteFunction}) => {
    return (
        <ul className={styles.todoList}>
            {items.map((item, idx) =>
                <TodoItem
                    value={item}
                    key={`${idx}-${item}`}
                    id={idx}
                    deleteFunction={deleteFunction} />
            )}
        </ul>
    )
}

export default TodoList