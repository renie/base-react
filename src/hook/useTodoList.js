import { useState } from 'react'
import TodoListService from '../service/TodoListService.js'

const useTodoList = () => {
    const service = TodoListService()
    const [ todoList, setTodoList ] = useState(service.getList())

    const addTodoItem = (value) => {
        service.addItem(value)
        setTodoList(service.getList())
    }

    const removeTodoItem = (id) => {
        service.removeItem(id)
        setTodoList(service.getList())
    }

    return {
        todoList,
        addTodoItem,
        removeTodoItem,
    }
}

export default useTodoList