const TodoListService = () => {
    const listName = 'list'

    const getList = () =>
        JSON.parse(localStorage.getItem(listName) || '[]')

    const saveList = (list) =>
        localStorage.setItem(listName, JSON.stringify(list))

    const addItem = (value) =>
        saveList([ ...getList(), value ])
    
    const removeItem = (id) =>
        saveList(getList().filter((_, idx) => idx !== id))

    return {
        getList,
        addItem,
        removeItem
    }
}

export default TodoListService